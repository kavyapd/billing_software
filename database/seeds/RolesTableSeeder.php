<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('roles')->truncate();
        
        $roles = [
            ['role' => 'admin'],
            ['role' => 'user'], 
        ];
        DB::table('roles')->insert($roles);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
