<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group([
    'prefix' => 'auth',
    'middleware' => ['cors','XSS']
], function(){
    Route::post('login', 'User\LoginController@login');
   
    Route::group([
        'middleware' => 'auth:api'
    ], function(){
       Route::post('addUser', 'User\AdminDashboardController@addUserDetails')->middleware('CheckAdminRole');
       Route::get('getUsers', 'User\AdminDashboardController@getUserDetails')->middleware('CheckAdminRole');
       Route::post('addItem', 'User\UserDashboardController@addItems');
       Route::get('getItem', 'User\UserDashboardController@getItemDetails');
//       Route::get('getItem/{id}', 'User\DashboardController@getItemDetailsById')->where('id', '[0-9]+');
       Route::get('getUser/details/{id}', 'User\AdminDashboardController@getUserDetailsById')->where('id', '[0-9]+')->middleware('CheckAdminRole');
       Route::get('getItemNames', 'User\UserDashboardController@getItemNames');
       Route::post('save/bill-generate/details', 'User\UserDashboardController@saveBillGeneartedItems');       
       Route::get('get/bill-generate/details', 'User\UserDashboardController@getBilledItemList');       
       Route::get('get-item-list/{id}', 'User\UserDashboardController@getBilledItemsListById')->where('id', '[0-9]+');       

       Route::get('sample/bill-generate/details', 'User\UserDashboardController@getSampleList')->middleware('CheckUserRole');       
       Route::get('pdf-generate/{id}', 'User\UserDashboardController@pdfGenerate')->where('id', '[0-9]+');       
       Route::post('update/user/details', 'User\AdminDashboardController@updateUserDetails')->middleware('CheckAdminRole');
       Route::delete('delete-user/{id}','User\AdminDashboardController@deleteUser')->where('id', '[0-9]+')->middleware('CheckAdminRole');
       Route::post('update/item/details', 'User\UserDashboardController@updateItemDetails');
       Route::delete('delete-item/{id}','User\UserDashboardController@deleteItem')->where('id', '[0-9]+');
       Route::get('get/top/quantity-list', 'User\AdminDashboardController@getQuantityDetails')->middleware('CheckAdminRole');
       Route::get('get/quantity-details', 'User\AdminDashboardController@getQuantityDetailsById')->middleware('CheckAdminRole');


       
       
    });
});












//     Route::get('get/list/itemBilled', 'User\UserDashboardController@getListofItemsBilled')->middleware('CheckUserRole');       
//     Route::get('bill-generate/details', 'User\UserDashboardController@billGenerateforSelectedItems')->middleware('CheckUserRole');       
      