<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user_role_result = DB::table('users')
                    ->select('users.id')
                    ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                    ->where('users.id',Auth::user()->id)
                    ->where('roles.role','user')
                    ->get();
            $user_role_result = json_decode($user_role_result, true);
            if(!empty($user_role_result) && is_array($user_role_result) && sizeof($user_role_result) > 0){
                return $next($request);
            }else{
                return response()->json([
                    'message' => 'Unauthorized'
                ], 401);
            }
        }else{
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }   
    }
}
