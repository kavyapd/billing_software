<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;

class AdminDashboardController extends Controller
{
 //admin can access to add the user details
    public function addUserDetails(Request $request){
        if(Auth::check()){
                $request->validate([
                    'name' => 'required|string|max:255',
                    'email' => 'required|email|max:255',
                    'password' => 'required',
                ]);
                DB::beginTransaction();
                $email_id_check = DB::table('users')
                     ->select('users.id','roles.role','users.account_active_status')
                    ->leftJoin('roles','roles.id','=','users.role_id')
                    ->where('users.email',$request->email)
                    ->get();
                $email_id_check = json_decode($email_id_check,true);
                if(!empty($email_id_check) && is_array($email_id_check) && sizeof($email_id_check)){
                    if(!empty($email_id_check['0']['role']) && $email_id_check['0']['role'] == 'user'){
                        if(!empty($email_id_check['0']['account_active_status'])){
                            return response()->json([
                                'message' => 'User has already added',
                                'success' => false,
                            ], 200);      
                        }else {
                            $users_table_update = DB::table('users')
                                    ->where('users.email',$request->email)
                                    ->update([
                                        'name' => $request->name,
                                        'email' => strtolower($request->email),
                                        'password' => bcrypt(strtolower($request->password)),
                                        'role_id' => DB::table('roles')->where('role','user')->first()->id,
                                        'account_active_status'=>true,
                                    ]);
                            if($users_table_update){
                            DB::commit();
                                return response()->json([
                                    'message' => 'User created successfully',
                                    'success' => true,
                                ], 200);  
                            }
                            return response()->json([
                                'message' => 'Something went wrong',
                                 'success' => false,
                             ], 200);
                        }
                    }else{
                        return response()->json([
                            'message' => 'Email ID already exists',
                            'success' => false,
                        ], 200);
                    }    
                }else {
                     $users_insert_result = new User([
                            'name' => $request->name,
                            'email' => strtolower($request->email),
                            'password' => bcrypt(strtolower($request->password)),
                            'role_id' => DB::table('roles')->where('role','user')->first()->id,
                            'account_active_status'=>true,

                        ]);
                        $users_insert_result->save();
                        DB::commit();
                        return response()->json([
                            'message' => 'User created successfully',
                            'success' => true,
                        ], 200);  
                }
        }
    }
      
  //update user details
    public function updateUserDetails(Request $request){
        if(Auth::check()){
            $request->validate([
                'id' => array('required','integer','exists:users,id',new \App\Rules\CheckUserRole()),
                'name' => 'required|string|max:255',
                'password' => 'required|string|max:255',
            ]);
            DB::beginTransaction();
            $user_updated_result = DB::table('users')
                        ->where('id',$request->id)       
                        ->where('users.account_active_status',1)
                        ->update(['name' => $request->name,'password' =>  bcrypt(strtolower($request->password)),'updated_at' => Carbon::now()]);
                if($user_updated_result){
                    DB::commit();
                    return response()->json([
                        'message' => 'User Updated successfully',
                        'success' => true,
                    ], 200);
                }else {
                    return response()->json([
                            'message' => 'User id missing',
                            'success' => true,
                     ], 200); 
                }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
  //delete user details  
    public function deleteUser(Request $request){
        if(Auth::check()){
            if($request->route('id')){
                $user_check_result = DB::table('users')
                    ->select('users.id')
                    ->leftJoin('roles','roles.id','=','users.role_id')
                    ->where('users.id',$request->route('id'))
                    ->where('roles.role','user')
                    ->get();
                $user_check_result = json_decode($user_check_result);
                if(!empty($user_check_result) && is_array($user_check_result) && sizeof($user_check_result)){
                    DB::beginTransaction();
                    $deleted_user_affected = DB::table('users')
                            ->where('id',$request->route('id'))
                            ->where('account_active_status',true)
                            ->update(['account_active_status'=>false]);
                    if($deleted_user_affected){
                        DB::commit();
                        return response()->json([
                            'message' => 'User deleted successfully',
                            'success' => true,
                        ], 200);
                    }
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 200);
                }else{
                    return response()->json([
                        'message' => 'User not found',
                        'success' => false,
                    ], 404);
                }
            }else{
                return response()->json([
                    'message' => 'User ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
 //fetch user details /get user details   
    public function getUserDetails(){
        if(Auth::check()){
            return response()->json([
                'data' => DB::table('users')
                       ->select('users.id','users.name','users.email','roles.role')
                       ->where('roles.role','user')
                       ->where('users.account_active_status',1)
                       ->leftJoin('roles','roles.id','=','users.role_id')
                       ->get(),
                'success' => true,
            ], 200);
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
 //fetch user details /get user details   
    public function getUserDetailsById(Request $request){
        if(Auth::check()){
            if($request->route('id')){
                $details_array = array();
                $bill_details = DB::table('bill_generates')
                        ->select('bill_generates.customer_id','add_items.user_id','customers.customer_name','customers.bill_id','customers.updated_at')
                        ->leftJoin('add_items','add_items.id','=','bill_generates.item_id')
                        ->leftJoin('users','users.id','=','add_items.user_id')
                        ->leftJoin('customers','customers.id','=','bill_generates.customer_id')
                        ->where('add_items.user_id',$request->route('id'))
                        ->get();
                $bill_details = json_decode($bill_details,true);
            if(!empty($bill_details) && is_array($bill_details) && sizeof($bill_details) > 0){
                foreach ($bill_details as $value) {
                   if(!empty($value['customer_id'])){
                        if(!isset($details_array[$value['customer_id']])){
                            $details_array[$value['customer_id']]=[
                                "customer_id" => $value['customer_id'],
                                "customer_name" => $value['customer_name'],
                                "bill_id" => $value['bill_id'],
                                "updated_at"=> $value['updated_at'],
                            ];
                        }
                    }
                }
            }
            sort($details_array);
            return response()->json([
                'data' => $details_array,
                'success' => true,
            ], 200);
            }else{
                return response()->json([
                    'message' => 'User ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
    public function getQuantityDetails(Request $request){
        if(Auth::check()){
            $details_array = array();
                $bill_details = DB::table('bill_generates')
                        ->select('bill_generates.id as bill_generated_id','bill_generates.customer_id','customers.customer_name','customers.bill_id',
                                 'customers.customer_name','customers.updated_at','bill_generates.item_id','add_items.item_name','bill_generates.quantity','users.name as user_name','add_items.cost','bill_generates.item_cost',
                                 'bill_generates.item_gst','bill_generates.total_cost_per_item','bill_generates.sell_price')
                        ->leftJoin('add_items','add_items.id','=','bill_generates.item_id')
                        ->leftJoin('users','users.id','=','add_items.user_id')
                        ->leftJoin('customers','customers.id','=','bill_generates.customer_id')
                        ->where('add_items.item_active_status',1)
                        ->get();
                $bill_details = json_decode($bill_details,true);
                $total_amount=0;
                 if(!empty($bill_details) && is_array($bill_details) && sizeof($bill_details) > 0){
                    foreach ($bill_details as $value) {
                       if(!empty($value['item_name'])){
                            if(!isset($details_array[$value['item_name']])){
                                $details_array[$value['item_name']]=[
                                    "item_name" => $value['item_name'],
                                    "total_quantity"=>0
                                ];
                           }
                           if(isset($details_array[$value['item_name']]) && !empty($value['quantity'])){
                               $details_array[$value['item_name']]['total_quantity']+=$value['quantity'];
                           }
                        }
                    }
                }
                sort($details_array);
                return response()->json([
                        'data' => $details_array,
                        'success' => true,
                    ], 200);
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
     public function getQuantityDetailsById(Request $request){
        if(Auth::check()){
                $details_array = array();
                $bill_details = DB::table('bill_generates')
                        ->select('add_items.item_name','add_items.category','bill_generates.quantity')
                        ->leftJoin('add_items','add_items.id','=','bill_generates.item_id')
                        ->leftJoin('users','users.id','=','add_items.user_id')
                        ->leftJoin('customers','customers.id','=','bill_generates.customer_id')
                        ->where('add_items.item_active_status',1)
                        ->get();
                
                $bill_details = json_decode($bill_details,true);
                if(!empty($bill_details) && is_array($bill_details) && sizeof($bill_details) > 0){
                    foreach ($bill_details as $value) {
                       if(!empty($value['item_name'])){
                            if(!isset($details_array[$value['item_name']])){
                                $details_array[$value['item_name']]=[
                                    "item_name" => $value['item_name'],
                                    "category" => $value['category'],
                                    "quantity" => $value['quantity'],
                                ];
                           }
                        } 
                    }
                }
                sort($details_array);
                return response()->json([
                        'data' => $bill_details,
                        'success' => true,
                    ], 200);
            
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}
     













// $total_amount = $total_amount + $value['quantity'];
//                            if(!empty($value['quantity'])){
//                                if(!in_array($value['quantity'], array_column($details_array[$value['quantity']]['total_quantity'], 'item_name'))){
//                                    $details_array[$value['quantity']]['total_quantity'][]=[
//                                        "quantity"=> $total_amount,
//                                    ];
//                                }
//                            }

//            
//        }
//        }
//        }
//            $details_array = array();
//            $get_top_quantity_details = DB::table('add_items')
//                        ->select('add_items.id','add_items.item_name','add_items.category')
//                        ->where('add_items.item_active_status',1)
//                        ->get();
//            $get_top_quantity_details = json_decode($get_top_quantity_details,true);
//            $get_quantity = DB::table('bill_generates')
//                        ->select('bill_generates.quantity')
//                        ->leftJoin('add_items','add_items.id','=','bill_generates.item_id')
//                        ->where('add_items.item_name',$get_top_quantity_details['0']['item_name'])
//                        ->get();
//            
//            $get_quantity = json_decode($get_quantity,true);
//                if(!empty($bill_details) && is_array($bill_details) && sizeof($bill_details) > 0){
//                    foreach ($bill_details as $value) {
//                       if(!empty($value['customer_id'])){
//                            if(!isset($details_array[$value['customer_id']])){
//                                
//                            }
//            $get_quantity = json_decode($get_quantity,true);
//            foreach ($get_quantity as $key => $get_quantity_value){
//                return response()->json([
//                    'data' => $get_quantity,
//                    'success' => true,
//                ], 200);
////            }
//            }
//        }else{
//            return response()->json([
//                'message' => 'User doesnt have access',
//                'success' => false,
//            ], 401);
//        }
//    }
//}




// $bill_generate_details = DB::table('bill_generates')
//                        ->select('bill_generates.customer_id','add_items.user_id','customers.customer_name','customers.bill_id','customers.updated_at')
//                        ->leftJoin('add_items','add_items.id','=','bill_generates.item_id')
//                        ->leftJoin('users','users.id','=','add_items.user_id')
//                        ->leftJoin('customers','customers.id','=','bill_generates.customer_id')
//                        ->where('add_items.user_id',$request->route('id'))
//                        ->get(); 

// if(!empty($bill_details) && is_array($bill_details) && sizeof($bill_details) > 0){
//                    foreach ($bill_details as $value) {
//                       if(!empty($value['customer_id'])){
//                            if(!isset($details_array[$value['customer_id']])){
//                                $details_array[$value['customer_id']]=[
//                                    "customer_id" => $value['customer_id'],
//                                    "customer_name" => $value['customer_name'],
//                                    "bill_id" => $value['bill_id'],
//                                    "updated_at"=> $value['updated_at'],
//                                ];
//                            }
//                       }
//                    }
//  
//                              }
//                sort($details_array);
