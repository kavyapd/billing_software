<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Model\Dashboard;
use App\Model\BillGenerate;
use App\Model\Customers;
use Carbon\Carbon;
use Validator;
use App;

class UserDashboardController extends Controller
{
  //user can access to add item details  
    public function addItems(Request $request){
        if(Auth::check()){
            $request->validate([
                'item_name' => 'required|string|max:255',
                'category' => 'required|string',
                'cost' => 'required|integer',
                'gst' => 'required|integer',
                'total_quantity' => 'required|integer',
            ]);
           DB::beginTransaction();
            $item_category_check = DB::table('add_items')
                   ->select('add_items.item_name','add_items.category','add_items.item_active_status')
                   ->where('add_items.category',$request->category)
                   ->where('add_items.item_name',$request->item_name)
                   ->where('add_items.user_id',Auth::user()->id)
                   ->get();
           $item_category_check = json_decode($item_category_check,true);
            if(!empty($item_category_check) && is_array($item_category_check) && sizeof($item_category_check)){
                if(!empty($item_category_check['0']['item_active_status'])){
                    return response()->json([
                        'message' => 'Item has already added',
                        'success' => false,
                    ], 200);   
                }else{
                    $item_details = DB::table('add_items')
                                   ->where('add_items.category',$request->category)
                                   ->update([
                                        'item_name' => $request->item_name,
                                        'category' => $request->category,
                                        'cost' => $request->cost,
                                        'gst' => $request->gst,
                                        'user_id'=> Auth::user()->id,
                                        'total_quantity'=>$request->total_quantity,
                                        'item_active_status'=>true,
                                   ]);
                        if($item_details){
                           DB::commit();
                               return response()->json([
                                   'message' => 'Item created successfully',
                                   'success' => true,
                               ], 200);  
                        }
                    return response()->json([
                        'message' => 'Something went wrong',
                         'success' => false,
                     ], 200);
                }
            }else{
                    $item_details = new Dashboard([
                        'item_name' => $request->item_name,
                        'category' => $request->category,
                        'cost' => $request->cost,
                        'gst' => $request->gst,
                        'total_quantity'=>$request->total_quantity,
                        'user_id'=> Auth::user()->id,
                    ]);
                    $item_details->save();
                    DB::commit();
                    return response()->json([
                        'message' => 'Item added successfully',
                        'success' => true,
                    ], 200);  

            }
        }
    }
    
   //user can update item details  
    public function updateItemDetails(Request $request){
        if(Auth::check()){
            $request->validate([
                'id' =>'required|integer|exists:add_items,id',
                'cost' => 'required|integer',
                'gst' => 'required|integer',
                'total_quantity' => 'required|integer',

            ]);
            DB::beginTransaction();
            $user_updated_result = DB::table('add_items')
                        ->where('id',$request->id)      
                        ->where('add_items.item_active_status',1)
                        ->update(['cost' => $request->cost,'gst' => $request->gst,'total_quantity' => $request->total_quantity,'updated_at' => Carbon::now()]);
                if($user_updated_result){
                    DB::commit();
                    return response()->json([
                        'message' => 'Item Updated successfully',
                        'success' => true,
                    ], 200);
                }else {
                    return response()->json([
                            'message' => 'Item id missing',
                            'success' => true,
                     ], 200); 
                }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
    
   //user can delete item details  
    public function deleteItem(Request $request){
        if(Auth::check()){
            if($request->route('id')){
                $item_check_result = DB::table('add_items')
                    ->select('add_items.id')
                    ->where('add_items.id',$request->route('id'))
//                    ->where('add_items.user_id',Auth::user()->id)
                    ->get();
                $item_check_result = json_decode($item_check_result);
                if(!empty($item_check_result) && is_array($item_check_result) && sizeof($item_check_result)){
                    DB::beginTransaction();
                    $deleted_item_affected = DB::table('add_items')
                            ->where('id',$request->route('id'))
                            ->where('item_active_status',true)
                            ->update(['item_active_status'=>false]);
                    if($deleted_item_affected){
                        DB::commit();
                        return response()->json([
                            'message' => 'Item deleted successfully',
                            'success' => true,
                        ], 200);
                    }
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 200);
                }else{
                    return response()->json([
                        'message' => 'User not found',
                        'success' => false,
                    ], 404);
                }
            }else{
                return response()->json([
                    'message' => 'Item ID is missing',
                    'success' => false,
                ], 400);
            }
        }else{
            return response()->json([
                'message' => 'Item doesnt have access',
                'success' => false,
            ], 401);
        }
    }
   
  //get item details 
    public function getItemDetails(){
        if(Auth::check()){
            return response()->json([
                'data' => DB::table('add_items')
                        ->select('add_items.id','add_items.user_id','users.name','add_items.item_name','add_items.category','add_items.cost','add_items.gst','add_items.total_quantity')
                        ->leftJoin('users','users.id','=','add_items.user_id')
                        ->where('add_items.item_active_status',1)
                        ->get(),
                'success' => true,
            ], 200);
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
 
  //get item names details 
    public function getItemNames(Request $request){
        if(Auth::check()){
            $get_item_names = DB::table('add_items')
                        ->where('add_items.item_active_status',1)
                        ->get();
                return response()->json([
                    'data' => $get_item_names,
                    'success' => true,
                ], 200);
        }
     }

 //bill generated details    
    public function saveBillGeneartedItems(Request $request){
        if(Auth::check()){
            $request->validate([
                'customer_name' => 'required|string|max:255',
                'bill_generated_items' => 'required|array',
                'bill_generated_items.*.item_id' => 'required|exists:add_items,id|regex:/^\d*(\.\d{1,10})?$/',
                'bill_generated_items.*.quantity' => 'required|regex:/^\d*(\.\d{0,10})?$/',
                'bill_generated_items.*.item_cost' => 'required|regex:/^\d*(\.\d{0,10})?$/',
                'bill_generated_items.*.item_gst' => 'required|regex:/^\d*(\.\d{0,10})?$/',
                'bill_generated_items.*.total_cost_per_item' => 'required|regex:/^\d*(\.\d{0,10})?$/',
                'bill_generated_items.*.sell_price' => 'regex:/^\d*(\.\d{0,10})?$/',
            ]);
            DB::beginTransaction();
            
            $save_customer_details = new Customers([
                'customer_name' => $request->customer_name,
            ]);
            if($save_customer_details->save()){
                $bill_generated_items_details = $request->bill_generated_items;
                $item_count=0; $item_array_count = 0;
                $item_array_count = sizeof($request->bill_generated_items);
                $get_customer_id =  $save_customer_details->id;
                $startId = 1000 + $save_customer_details->id;
                DB::table('customers')->where('id',$save_customer_details->id)->update(['bill_id'=>$startId]);
                foreach ($bill_generated_items_details as $key => $value){
                    $bill_genearate_inserted_items_result = new BillGenerate([
                        'item_id' => $value['item_id'],
                        'quantity' => $value['quantity'],
                        'item_cost' => $value['item_cost'],
                        'item_gst' => $value['item_gst'],
                        'total_cost_per_item' => $value['total_cost_per_item'],
                        'sell_price' => !empty($value['sell_price']) ? $value['sell_price'] : NULL ,
                        'customer_id' =>$get_customer_id,
                    ]);
                    if($bill_genearate_inserted_items_result->save()){
                        if($bill_genearate_inserted_items_result->id){
                            $item_count++;
                        }
                    }
                }
                if($item_count == $item_array_count){
                    DB::commit();
                }
                $get_bill_id_details = DB::table('customers')
                    ->select('id','bill_id')
                    ->where('customers.id',$get_customer_id)
                    ->get();
                return response()->json([
                        'message' => $get_bill_id_details,
                        'success' => true,
                    ], 200);
                
            }else{
                return response()->json([
                        'message' => 'Something went wrong',
                        'success' => false,
                    ], 400);
            }
         }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
 
 //pdf generate for particular id   
    public function pdfGenerate(Request $request){
        if(Auth::check()){
            if($request->route('id')){
                DB::beginTransaction();
                    $bill_generate_details = DB::table('bill_generates')
                        ->select('bill_generates.item_id','add_items.item_name','bill_generates.quantity','users.name as user_name','add_items.cost','bill_generates.item_cost',
                                 'bill_generates.item_gst','bill_generates.total_cost_per_item','bill_generates.sell_price','customers.customer_name')
                        ->leftJoin('add_items','add_items.id','=','bill_generates.item_id')
                        ->leftJoin('users','users.id','=','add_items.user_id')
                        ->leftJoin('customers','customers.id','=','bill_generates.customer_id')
                        ->where('bill_generates.customer_id',$request->route('id'))
                        ->get();
                    $bill_generate_details = json_decode($bill_generate_details,true);
                    if(!empty($bill_generate_details) && is_array($bill_generate_details) && sizeof($bill_generate_details)){
                        //pdf template
                        $pdf = App::make('dompdf.wrapper');
                            $template='<html><style>
                                table{
                                    border-collapse: collapse;
                                }
                                .main-tr td{
                                    padding:8px 2px;
                                    border-bottom: 2px solid black;
                                    border-top: 2px solid black;
                                }
                                .main-tr td p{
                                    font-weight:800;
                                    font-size:17px;
                                    color: black;
                                }
                                .total-amount td{
                                    padding:10px 2px 30px 2px;
                                    border-bottom: 2px solid black;
                                    border-top: 2px solid black;
                                }
                                .sub-tr td{
                                        padding:8px 2px;
                                        border: none;
                                }
                                .table{
                                        margin-top: 30px;
                                        width:100%;
                                        overflow:auto;
                                }
                                .bill-detail{
                                        width: 100%;
                                }
                            </style>
                                <body>
                                    <div class="details">';
                            $get_customer_name = DB::table('bill_generates')
                                    ->leftJoin('customers','customers.id','=','bill_generates.customer_id')
                                    ->where('bill_generates.customer_id',$request->route('id'))
                                    ->get();
                            $get_customer_name = json_decode($get_customer_name,true);
                                $template.= '<div style="text-align:center;">
                                            <b style="color:#000;font-size:18px;text-align:center;">BILL</b><br><br>
                                        </div>
                                        <div>
                                            <span style="color:#000;font-size:18px;">Bill No: '.(!empty($get_customer_name['0']['bill_id']) ? $get_customer_name['0']['bill_id'] : '').'</span>
                                            <span style="float:right;color:#000;font-size:18px;">Date: '.(!empty($get_customer_name['0']['updated_at']) ? $get_customer_name['0']['updated_at'] : '').'</span>
                                        </div>
                                        <div>
                                            <span style="color:#000;font-size:18px;">Customer Name:'.(!empty($get_customer_name['0']['customer_name']) ? $get_customer_name['0']['customer_name'] : '').'</span>
                                        </div>';

                                $template.= '<div class="table">
                                            <table class="bill-detail">
                                                    <tr class="main-tr">
                                                        <td><p style="text-align:center;">Sl.No</p></td>
                                                        <td><p style="text-align:center;">Item Name</p></td>
                                                        <td><p style="float:right;">MRP</p></td>
                                                        <td><p style="float:right;">Quantity</p></td>
                                                        <td><p style="float:right;">GST</p></td>
                                                        <td><p style="float:right;">Total Price</p></td>
                                                        <td><p style="float:right;">Sell Price</p></td>
                                                    </tr>';
                                $total_amount=0;
                            foreach ($bill_generate_details as $key => $bill_generate_details_value){
                                $template.= '<tr class="sub-tr">
                                                <td><p style="text-align:center;">'.($key + 1).'</td>
                                                <td><p style="text-align:center;">'.(!empty($bill_generate_details_value['item_name']) ? $bill_generate_details_value['item_name'] : '').'</p></td>
                                                <td><p style="float:right;">'.(!empty($bill_generate_details_value['cost']) ? $bill_generate_details_value['cost'] : '').'</p></td>
                                                <td><p style="float:right;">'.(!empty($bill_generate_details_value['quantity']) ? $bill_generate_details_value['quantity'] : '').'</p></td>
                                                <td><p style="float:right;">'.(!empty($bill_generate_details_value['item_gst']) ? $bill_generate_details_value['item_gst'] : '').'</p></td>
                                                <td><p style="float:right;">'.(!empty($bill_generate_details_value['total_cost_per_item']) ? $bill_generate_details_value['total_cost_per_item'] : '').'</p></td>
                                                <td><p style="float:right;">'.(!empty($bill_generate_details_value['sell_price']) ? $bill_generate_details_value['sell_price'] : '').'</p></td>
                                            </tr>';
                                $total_amount = $total_amount + $bill_generate_details_value['sell_price'];
                            }
                            $template.='<tr class="total-amount">
                                            <td colspan="5"></td>
                                            <td colspan="2"><b style="float:right; padding-right:20px;">Total Amount:'.$total_amount.'</b></td>
                                        </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </body>
                                </html>';
                            $pdf->loadHTML($template);
                            return $pdf->stream();          
                        }
                        return response()->json([
                            'message' => 'Something went wrong',
                            'success' => false,
                        ], 400);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
   }
  
 //get bill generated item list  
   public function getBilledItemList(Request $request){
        if(Auth::check()){
            $get_billed_item_list_array = array();
            $get_billed_item_list = DB::table('bill_generates')
                        ->select('bill_generates.customer_id','add_items.user_id','customers.customer_name','customers.bill_id','customers.updated_at')
                        ->leftJoin('add_items','add_items.id','=','bill_generates.item_id')
                        ->leftJoin('users','users.id','=','add_items.user_id')
                        ->leftJoin('customers','customers.id','=','bill_generates.customer_id')
                        ->where('add_items.user_id',Auth::user()->id)
                        ->get();
          $get_billed_item_list = json_decode($get_billed_item_list,true);
            if(!empty($get_billed_item_list) && is_array($get_billed_item_list) && sizeof($get_billed_item_list) > 0){
                foreach ($get_billed_item_list as $value) {
                   if(!empty($value['customer_id'])){
                        if(!isset($get_billed_item_list_array[$value['customer_id']])){
                            $get_billed_item_list_array[$value['customer_id']]=[
                                "customer_id" => $value['customer_id'],
                                "customer_name" => $value['customer_name'],
                                "bill_id" => $value['bill_id'],
                                "updated_at"=> $value['updated_at'],
                            ];
                        }
                    }
                }
            }
            sort($get_billed_item_list_array);
            return response()->json([
                'data' => $get_billed_item_list_array,
                'success' => true,
            ], 200);
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
     }
     
  //get bill generated item list by id
    public function getBilledItemsListById(Request $request){
        if(Auth::check()){
            if($request->route('id')){
                $details_array = array();
                $bill_details = DB::table('bill_generates')
                        ->select('bill_generates.customer_id','customers.customer_name','customers.bill_id',
                                 'customers.customer_name','customers.updated_at','bill_generates.item_id','add_items.item_name','bill_generates.quantity','users.name as user_name','add_items.cost','bill_generates.item_cost',
                                 'bill_generates.item_gst','bill_generates.total_cost_per_item','bill_generates.sell_price')
                        ->leftJoin('add_items','add_items.id','=','bill_generates.item_id')
                        ->leftJoin('users','users.id','=','add_items.user_id')
                        ->leftJoin('customers','customers.id','=','bill_generates.customer_id')
                        ->where('bill_generates.customer_id',$request->route('id'))
                        ->get();
                
                $bill_details = json_decode($bill_details,true);
                if(!empty($bill_details) && is_array($bill_details) && sizeof($bill_details) > 0){
                    foreach ($bill_details as $value) {
                       if(!empty($value['customer_id'])){
                            if(!isset($details_array[$value['customer_id']])){
                                $details_array[$value['customer_id']]=[
                                    "customer_id" => $value['customer_id'],
                                    "customer_name" => $value['customer_name'],
                                    "bill_id" => $value['bill_id'],
                                    "updated_at"=> $value['updated_at'],
                                    "items_details"=>[]
                                ];
                           }
                            if(!empty($value['customer_id'])){
                                if(!in_array($value['customer_id'], array_column($details_array[$value['customer_id']]['items_details'], 'customer_id'))){
                                    $details_array[$value['customer_id']]['items_details'][]=[
                                        "item_id" => $value['item_id'],
                                        "item_name"=> $value['item_name'],
                                        "quantity"=> $value['quantity'],
                                        "cost"=> $value['cost'],
                                        "item_cost"=> $value['item_cost'],
                                        "item_gst"=> $value['item_gst'],
                                        "total_cost_per_item"=> $value['total_cost_per_item'],
                                        "sell_price"=> $value['sell_price'],
                                    ];
                                }
                            }
                        } 
                    }
                }
                sort($details_array);
                return response()->json([
                        'data' => $details_array,
                        'success' => true,
                    ], 200);
            }
        }else{
            return response()->json([
                'message' => 'User doesnt have access',
                'success' => false,
            ], 401);
        }
    }
}

// $check_item_id = DB::table('add_item') ->select('add_items.cost')->where('add_items.user_id',Auth::user()->id);
//                   $check_item_id = json_decode($check_item_id,true);
//
//                    return response()->json([
//                        'message' => $value['quantity'] - $check_item_id['0']['cost'],
//                        'success' => true,
//                    ], 200);











