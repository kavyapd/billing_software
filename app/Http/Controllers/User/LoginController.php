<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    //login details
    public function login(Request $request){
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        $user_details = DB::table('users')->select('users.id','users.name','users.role_id','roles.role')
                        ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
                        ->where('users.id',$request->user()->id)
                        ->get();
        $user_details = json_decode($user_details, true);
        
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'success'=>true,
                'user'=>((!empty($user_details) && sizeof($user_details) > 0) ? $user_details[0] : [])
            ]);
    }
    
     /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        if($request && $request->user() && $request->user()->token()){
            $request->user()->token()->revoke();
            return response()->json([
                'message' => 'Successfully logged out',
                'success' => true,
            ]);
        }else{
            return response()->json([
                'message' => 'Something went wrong',
                'success' => false,
            ]);
        }
    }
}