<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    protected $table = 'add_items';
    
    protected $fillable = [
        'item_name','category','cost','gst','user_id','total_quantity',
    ];
    protected $guarded = ['id'];
    
    protected $hidden = [];
}
