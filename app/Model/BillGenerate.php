<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BillGenerate extends Model
{
    protected $table = 'bill_generates';
    protected $guarded = ['id'];
    protected $hidden = [];
}
