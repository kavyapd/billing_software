<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';
    protected $fillable = ['customer_name','bill_id'];
    protected $guarded = ['id'];
    protected $hidden = [];
}
